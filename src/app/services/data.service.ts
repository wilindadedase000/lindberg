import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {
  //apiLink: string = 'http://techmatesph.com/cse/apis/'; //techmatesph server
  apiLink: string = 'http://localhost/afp/apis/'; //localhost

  constructor(public http:HttpClient) { console.log('Data Service is working...') }

  pullData(data, vAPI){
    return this.http.post(this.apiLink+vAPI+'.php', btoa(JSON.stringify(data)));
  }
}
