import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { HttpClientModule } from '@angular/common/http';
import { CountdownTimerModule } from 'ngx-countdown-timer';

import { DataService } from './services/data.service';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { MealLayoutComponent } from './layouts/meal-layout/meal-layout.component';
import { FruitLayoutComponent } from './layouts/fruit-layout/fruit-layout.component';
import { LoginComponent } from './login/login.component';
import {CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';




@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    CountdownTimerModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    MealLayoutComponent,
    FruitLayoutComponent,
    LoginComponent

  ],
  providers: [ DataService ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
