import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { MealLayoutComponent } from './layouts/meal-layout/meal-layout.component';
import { FruitLayoutComponent } from './layouts/fruit-layout/fruit-layout.component';
import { LoginComponent } from './login/login.component';

const routes: Routes =[
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  }]},{
    path: '',
    component: MealLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/meal-layout/meal-layout.module#MealLayoutModule'
  }]},{
    path: '',
    component: FruitLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/fruit-layout/fruit-layout.module#FruitLayoutModule'
  }]},{
    path: '**',
    redirectTo: 'login'
  },

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
    

  ],
  exports: [
  ],
})
export class AppRoutingModule { }
