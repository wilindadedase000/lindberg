import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(private modalService: NgbModal) { }
  closeResult: string;

  ngOnInit() {
  }

  open(content){

    this.modalService.open(content, {'size': 'lg', 'centered': true }).result.then((result)=>{
      this.closeResult = 'Closed with: ${result}';
    }, (reason) =>{
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });
  }
  
  private getDismissReason(reason: any) : string {
    if(reason === ModalDismissReasons.ESC) {
      return 'by Pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by Clicking on a Backdrop';
    } else {
      return 'with: ${reasonm}';
    }
  }

}
