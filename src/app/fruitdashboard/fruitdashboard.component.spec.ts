import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitdashboardComponent } from './fruitdashboard.component';

describe('FruitdashboardComponent', () => {
  let component: FruitdashboardComponent;
  let fixture: ComponentFixture<FruitdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FruitdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
