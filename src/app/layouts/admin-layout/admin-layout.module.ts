import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

//components
// import { DashboardComponent } from '../../dashboard/dashboard.component';
import { FreemealComponent } from '../../freemeal/freemeal.component';
import { EmployeeComponent } from '../../employee/employee.component';
import { AccountsComponent } from '../../accounts/accounts.component';
import { MenuComponent } from '../../menu/menu.component';
import { IconsComponent } from '../../icons/icons.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    // DashboardComponent, 
    FreemealComponent,
    EmployeeComponent,
    AccountsComponent,
    MenuComponent,
    IconsComponent
  ]
})

export class AdminLayoutModule {}
