import { Routes } from '@angular/router';

// import { DashboardComponent } from '../../dashboard/dashboard.component';
import { FreemealComponent } from '../../freemeal/freemeal.component';
import { EmployeeComponent } from '../../employee/employee.component';
import { MenuComponent } from '../../menu/menu.component';
import { AccountsComponent } from '../../accounts/accounts.component';
import { IconsComponent } from '../../icons/icons.component';


export const AdminLayoutRoutes: Routes = [
    // { path: 'dashboard',component: DashboardComponent },
    { path: 'freemeal',component: FreemealComponent },
    { path: 'menu',component: MenuComponent },
    { path: 'employee',component: EmployeeComponent },
    { path: 'accounts',component: AccountsComponent },
    { path: 'icons',component: IconsComponent }

];
