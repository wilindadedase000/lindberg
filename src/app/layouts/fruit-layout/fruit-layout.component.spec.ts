import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitLayoutComponent } from './fruit-layout.component';

describe('FruitLayoutComponent', () => {
  let component: FruitLayoutComponent;
  let fixture: ComponentFixture<FruitLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FruitLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
