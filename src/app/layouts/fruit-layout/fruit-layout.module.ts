//meal-layout.module.ts

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FruitLayoutRoutes } from './fruit-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

//components

import { FruitreportComponent } from '../../fruitreport/fruitreport.component';
import { FruitdashboardComponent } from '../../fruitdashboard/fruitdashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FruitLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    FruitreportComponent, 
    FruitdashboardComponent

  ]
})

export class FruitLayoutModule {}