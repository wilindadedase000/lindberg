import { Routes } from '@angular/router';


import { FruitreportComponent } from '../../fruitreport/fruitreport.component';
import { FruitdashboardComponent } from '../../fruitdashboard/fruitdashboard.component';

export const FruitLayoutRoutes: Routes = [

    { path: 'fruitreport',component: FruitreportComponent },
    { path: 'fruitdashboard',component: FruitdashboardComponent }

];
