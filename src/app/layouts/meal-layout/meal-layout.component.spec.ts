import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealLayoutComponent } from './meal-layout.component';

describe('MealLayoutComponent', () => {
  let component: MealLayoutComponent;
  let fixture: ComponentFixture<MealLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
