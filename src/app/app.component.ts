import { Component} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    localStorage.setItem('tmcore', '[{"token":"ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUlzSW1Gd2NDSTZJa05UUlZJaUxDSmtaWFlpT2lKVVpXTm9UV0YwWlhOUVNDSjkuZXlKMVkyOWtaU0k2SW5SdE1EQXlJaXdpZFdWdFlXbHNJam9pYldWc2JtVnlZbUZzWTJWQVoyMWhhV3d1WTI5dElpd2lhV0o1SWpvaWRHVmphRzFoZEdWeklpd2lhV1VpT2lKcGJtWnZRSFJsWTJodFlYUmxjM0JvTG1OdmJTSXNJbWxrWVhSbElqcDdJbVJoZEdVaU9pSXlNREU0TFRBNExUSTVJREV6T2pBM09qTXdMamsyTWpJMk1pSXNJblJwYldWNmIyNWxYM1I1Y0dVaU9qTXNJblJwYldWNmIyNWxJam9pUlhWeWIzQmxYQzlDWlhKc2FXNGlmWDAubm5UQ3BmdVJLamNkTTlKeTZYWlhNaFhpT1VDZXEzODVYSlRjblR4Z3libw==", "vURL":"www.techmatesph.com"}]');
  }
}
