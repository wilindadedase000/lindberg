import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitreportComponent } from './fruitreport.component';

describe('FruitreportComponent', () => {
  let component: FruitreportComponent;
  let fixture: ComponentFixture<FruitreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FruitreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
