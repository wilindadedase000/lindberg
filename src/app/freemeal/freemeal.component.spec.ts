import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreemealComponent } from './freemeal.component';

describe('FreemealComponent', () => {
  let component: FreemealComponent;
  let fixture: ComponentFixture<FreemealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreemealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreemealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
