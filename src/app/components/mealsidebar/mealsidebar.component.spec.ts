import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsidebarComponent } from './mealsidebar.component';

describe('MealsidebarComponent', () => {
  let component: MealsidebarComponent;
  let fixture: ComponentFixture<MealsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
