import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/mealdashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
  { path: '/mealreport', title: 'Meal Reports',  icon: 'files_single-copy-04', class: '' }

];

@Component({
  selector: 'app-mealsidebar',
  templateUrl: './mealsidebar.component.html',
  styleUrls: ['./mealsidebar.component.scss']
})
export class MealsidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ( window.innerWidth > 991) {
        return false;
    }
    return true;
};
}
