import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitsidebarComponent } from './fruitsidebar.component';

describe('FruitsidebarComponent', () => {
  let component: FruitsidebarComponent;
  let fixture: ComponentFixture<FruitsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FruitsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
