import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/fruitdashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
  { path: '/fruitreport', title: 'Fruit Reports',  icon: 'files_single-copy-04', class: '' }

];

@Component({
  selector: 'app-fruitsidebar',
  templateUrl: './fruitsidebar.component.html',
  styleUrls: ['./fruitsidebar.component.scss']
})
export class FruitsidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ( window.innerWidth > 991) {
        return false;
    }
    return true;
};

}
